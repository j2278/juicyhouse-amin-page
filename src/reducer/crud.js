
const initialState = {
    crud: 0
}

const crudReducer = (state = initialState, action) => {
    switch(action.type) {
        case "RELOAD_ALL_DATA" : {
            return {
                ...state,
                crud: action.payload
            }
        }

        default :
        return state
    }
}

export default crudReducer