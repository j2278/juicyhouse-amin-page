
const initialState = {
    allCustomer: []
}

const customerReducer = (state = initialState, action) => {
    switch(action.type) {
        case "SET_ALL_CUSTOMER" : {
            return {
                ...state,
                allCustomer: action.payload
            }
        }

        default :
        return state
    }
}

export default customerReducer