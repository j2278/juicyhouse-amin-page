import {combineReducers} from "redux"
import customerReducer from "./customer"
import orderReducer from "./order"
import productReducer from "./product"
import crudReducer from "./crud"

const rootReducer = combineReducers({
    product: productReducer,
    customer: customerReducer,
    order: orderReducer,
    crud: crudReducer
})

export default rootReducer