
const initialState = {
    allProduct: [],
    originList: [
        {originName: "Choose product origin", originId: "NONE"},
        {originName: "England", originId: "England"},
        {originName: "America", originId: "America"},
        {originName: "China", originId: "China"},
        {originName: "France", originId: "France"},
        {originName: "Canada", originId: "Canada"},
    ],
    capacityList: [
        {capacityMl: "Choose product capacity", capacityId: 0},
        {capacityMl: "10 ML", capacityId: 10},
        {capacityMl: "30 ML", capacityId: 30},
        {capacityMl: "60 ML", capacityId: 60},
        {capacityMl: "100 ML", capacityId: 100},
    ],
    flavorList: [
        {flavorName: "Choose product flavor", flavorId: "NONE"},
        {flavorName: "Fruit", flavorId: "Fruit"},
        {flavorName: "Creamy", flavorId: "Creamy"},
        {flavorName: "Beverage", flavorId: "Beverage"},
    ],
    typeList: [
        {typeName: "Choose product type", typeId: "NONE"},
        {typeName: "Salt Nic Juice", typeId: "SALT-NIC"},
        {typeName: "Free Base Juice", typeId: "FREE-BASE"},
    ]
}

const productReducer = (state = initialState, action) => {
    switch(action.type) {
        case "SET_ALL_PRODUCT" : {
            return {
                ...state,
                allProduct: action.payload
            }
        }

        default :
        return state
    }
}

export default productReducer