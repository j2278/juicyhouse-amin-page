
const initialState = {
    allOrder: []
}

const orderReducer = (state = initialState, action) => {
    switch(action.type) {
        case "SET_ALL_ORDER" : {
            return {
                ...state,
                allOrder: action.payload
            }
        }

        default :
        return state
    }
}

export default orderReducer