import axios from "axios"
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import VisibilityIcon from '@mui/icons-material/Visibility';
import { Tooltip, Table, TableCell, TableRow, TableHead, TableBody, Button, Modal, Box, Typography, Grid, TextField, Alert, Snackbar, MenuItem, Menu } from "@mui/material"
import SettingsIcon from '@mui/icons-material/Settings';

const modalStyle = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 1100,
    height: 450,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const modalDelete = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    height: 200,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const inputStyle = {
    paddingRight: "20px"
}

function CustomerTable({ customerInPage, page, limit }) {
    const dispatch = useDispatch()
    const allCustomer = useSelector(state => state.customer.allCustomer);
    const crud = useSelector(state => state.crud.crud);
    const [openModal, setOpenModal] = useState(false);
    const [openDelete, setOpenDelete] = useState(false);
    const [errorDisplay, setErrorDisplay] = useState("none");
    const [openSuccess, setOpenSuccess] = useState(false);
    const [disable, setDisable] = useState(true)
    const [anchorEl, setAnchorEl] = useState(null);
    const openMenu = Boolean(anchorEl);

    const [customerInput, setCustomerInput] = useState({});

    const [error, setError] = useState({
        fullname: "none",
        email: "none",
        phone: "none",
        address: "none",
        city: "none",
        country: "none",
        avatar: "none"
    })

    const onNameChange = (event) => {
        setCustomerInput({ ...customerInput, fullname: event.target.value })

        if (event.target.value === "") {
            setError({ ...error, fullname: "Full name is empty!" })
        } else {
            setError({ ...error, fullname: "none" })
        }
    }

    const onEmailChange = (event) => {
        setCustomerInput({ ...customerInput, email: event.target.value })
        let emailListExist = allCustomer.filter(element => element.email === event.target.value)

        if (event.target.value === "") {
            setError({ ...error, email: "Email is empty!" });
        } else if (/\S+@\S+\.\S+/.test(event.target.value) === false) {
            setError({ ...error, email: "Email syntax is not valid!" });
        } else if (emailListExist.length > 0) {
            setError({ ...error, email: "This email is already exist!" })
        } else {
            setError({ ...error, email: "none" });
        }
    }

    const onPhoneChange = (event) => {
        setCustomerInput({ ...customerInput, phone: event.target.value })
        let phonelListExist = allCustomer.filter(element => element.phone === event.target.value)

        if (event.target.value === "") {
            setError({ ...error, phone: "Phone number is empty!" })
        } else if (isNaN(event.target.value) || event.target.value.length !== 10) {
            setError({ ...error, phone: "Phone number must be a 10 digit number!" })
        } else if (phonelListExist.length > 0) {
            setError({ ...error, phone: "This phone number is already exist!" })
        } else {
            setError({ ...error, phone: "none" })
        }
    }

    const onAddressChange = (event) => {
        setCustomerInput({ ...customerInput, address: event.target.value })

        if (event.target.value === "") {
            setError({ ...error, address: "Address is empty!" });
        } else {
            setError({ ...error, address: "none" });
        }
    }

    const onCityChange = (event) => {
        setCustomerInput({ ...customerInput, city: event.target.value })

        if (event.target.value === "") {
            setError({ ...error, city: "City is empty!" });
        } else {
            setError({ ...error, city: "none" });
        }
    }

    const onCountryChange = (event) => {
        setCustomerInput({ ...customerInput, country: event.target.value })

        if (event.target.value === "") {
            setError({ ...error, country: "Country origin is empty!" });
        } else {
            setError({ ...error, country: "none" });
        }
    }

    const onAvatarChange = (event) => {
        setCustomerInput({ ...customerInput, avatar: event.target.value })

        if (event.target.value === "") {
            setError({ ...error, avatar: "Avatar is empty!" });
        } else {
            setError({ ...error, avatar: "none" });
        }
    }

    const onSettingClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const closeMenu = () => {
        setAnchorEl(null);
    };

    const closeModal = () => {
        setOpenModal(false);
        setErrorDisplay("none");
        setDisable(true)
        setError({
            fullname: "none",
            email: "none",
            phone: "none",
            address: "none",
            city: "none",
            country: "none",
        })
    }

    const closeSuccess = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpenSuccess(false);
    };

    const onViewClick = (element) => {
        setOpenModal(true);
        setCustomerInput(element)
    }

    const onEditClick = () => {
        setDisable(false);
        closeMenu()
    }

    const onEraseClick = () => {
        setOpenDelete(true);
        closeMenu()
    }

    const closeDelete = () => {
        setOpenDelete(false);
    }

    const deleteCustomer = async () => {
        const response = await axios(`http://localhost:8000/customers/${customerInput._id}`, {
            method: `DELETE`
        });
        const data = response.data;
        return data;
    }

    const updateCustomer = async () => {
        let date = new Date();
        let today = `${date.getHours()}:${date.getMinutes()} ${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`
        let body = {
            fullname: customerInput.fullname,
            email: customerInput.email,
            phone: customerInput.phone,
            address: customerInput.address,
            city: customerInput.city,
            country: customerInput.country,
            avatar: customerInput.avatar,
            timeUpdated: today,
        }
        const response = await axios(`http://localhost:8000/customers/${customerInput._id}`, {
            method: `PUT`,
            data: body,
            header: { "Content-Type": "application/json;charset=UTF-8" }
        })
        const data = response.data;
        return data;
    }

    const onUpdateClick = () => {
        setErrorDisplay("block")
        if (error.fullname === "none" && error.email === "none" && error.phone === "none" 
        && error.address === "none" && error.city === "none" 
        && error.country === "none" && error.avatar === "none") {
                setOpenSuccess(true)
                closeModal()
                updateCustomer()
                    .then((data) => {
                        dispatch({ type: "RELOAD_ALL_DATA", payload: crud + 1 })
                    })
            }
        }

    const onDelete = () => {
        deleteCustomer()
            .then((data) => {
                setOpenSuccess(true);
                closeModal();
                closeDelete();
                dispatch({ type: "RELOAD_ALL_DATA", payload: crud + 1 })
            })
    }

    useEffect(() => {
        let errorFree = {
            fullname: "none",
            email:  "none",
            phone: "none",
            address: "none",
            city: "none",
            country: "none",
            avatar: "none"
        }
        if(customerInput.phone === ""){
            errorFree.phone = "Phone number is empty!";
        }
        if(customerInput.address === ""){
            errorFree.address = "Address is empty!"
        }
        if(customerInput.city === ""){
            errorFree.city = "City is empty!"
        }
        if(customerInput.country === ""){
            errorFree.country = "Country is empty!"
        }
        setError(errorFree)
    },[customerInput])
    return (
        <div>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell style={{ fontWeight: "bold" }} >No</TableCell>
                        <TableCell style={{ fontWeight: "bold" }}>Avatar</TableCell>
                        <TableCell style={{ fontWeight: "bold" }}>Full name</TableCell>
                        <TableCell style={{ fontWeight: "bold" }}>Email</TableCell>
                        <TableCell style={{ fontWeight: "bold" }}>Phone</TableCell>
                        <TableCell style={{ fontWeight: "bold" }}>Address</TableCell>
                        <TableCell style={{ fontWeight: "bold" }}>City</TableCell>
                        <TableCell style={{ fontWeight: "bold" }}>Country</TableCell>
                        <TableCell style={{ fontWeight: "bold" }}>View</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {customerInPage.map((element, index) => {
                        return <TableRow key={index}>
                            <TableCell>{(index + 1) + (limit * (page - 1))}</TableCell>
                            <TableCell> <img alt="" src={element.avatar} style={{ width: "50px", height: "50px" }}></img> </TableCell>
                            <TableCell>{element.fullname}</TableCell>
                            <TableCell>{element.email}</TableCell>
                            <TableCell>{element.phone}</TableCell>
                            <TableCell>{element.address}</TableCell>
                            <TableCell>{element.city}</TableCell>
                            <TableCell>{element.country}</TableCell>
                            <TableCell>
                                <VisibilityIcon onClick={() => onViewClick(element)} style={{ color: "black", marginRight: "5px" }} />
                            </TableCell>
                        </TableRow>
                    })}
                </TableBody>
            </Table>
            <Modal open={openModal} onClose={closeModal}>
                <Box sx={modalStyle}>
                <Grid container>
                        <Grid item xs={8} style={{ textAlign: "right", paddingRight: "30px" }}>
                            <Typography fontWeight="bold" variant="h6"> PRODUCT INFORMATION</Typography>
                        </Grid>
                        <Grid item xs={4} style={{ textAlign: "right" }}>
                            <Tooltip title="Setting"><SettingsIcon onClick={onSettingClick} /></Tooltip>
                            <Menu anchorEl={anchorEl} open={openMenu} onClose={closeMenu}>
                                <MenuItem onClick={onEditClick}> <EditIcon className="mx-2" /> Edit </MenuItem>
                                <MenuItem onClick={onEraseClick}> <DeleteIcon className="mx-2" /> Delete </MenuItem>
                            </Menu>
                        </Grid>
                    </Grid>
                    <Grid container className="my-4">
                        <Grid item xs={4}>
                            <TextField value={customerInput.fullname} disabled={disable} onChange={onNameChange} style={inputStyle} size="small" fullWidth label="Full name" variant="outlined"></TextField>
                            {error.fullname === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.fullname}</Typography>}
                        </Grid>
                        <Grid item xs={4}>
                            <TextField value={customerInput.email} disabled={disable} onChange={onEmailChange} style={inputStyle} size="small" fullWidth label="Email" variant="outlined"></TextField>
                            {error.email === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.email}</Typography>}
                        </Grid>
                        <Grid item xs={4}>
                            <TextField value={customerInput.phone} disabled={disable} onChange={onPhoneChange} style={inputStyle} size="small" fullWidth label="Phone" variant="outlined"></TextField>
                            {error.phone === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.phone}</Typography>}
                        </Grid>
                    </Grid>
                    <Grid container className="my-4">
                        <Grid item xs={4}>
                            <TextField value={customerInput.address} disabled={disable} onChange={onAddressChange} style={inputStyle} size="small" fullWidth label="Address" variant="outlined"></TextField>
                            {error.address === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.address}</Typography>}
                        </Grid>
                        <Grid item xs={4}>
                            <TextField value={customerInput.city} disabled={disable} onChange={onCityChange} style={inputStyle} size="small" fullWidth label="City" variant="outlined"></TextField>
                            {error.city === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.city}</Typography>}
                        </Grid>
                        <Grid item xs={4}>
                            <TextField value={customerInput.country} disabled={disable} onChange={onCountryChange} style={inputStyle} size="small" fullWidth label="Country" variant="outlined"></TextField>
                            {error.country === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.country}</Typography>}
                        </Grid>
                    </Grid>
                    <Grid container className="my-4">
                        <Grid item xs={4}>
                            <TextField value={customerInput.avatar} disabled={disable} onChange={onAvatarChange} style={inputStyle} size="small" fullWidth label="Avatar" variant="outlined"></TextField>
                            {error.avatar === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.avatar}</Typography>}
                        </Grid>
                        <Grid item xs={4}>
                            <TextField value={customerInput.timeCreated} disabled style={inputStyle} size="small" fullWidth label="Time created" variant="outlined"></TextField>
                        </Grid>
                        <Grid item xs={4}>
                            <TextField value={customerInput.timeUpdated} disabled style={inputStyle} size="small" fullWidth label="Time updated" variant="outlined"></TextField>
                        </Grid>
                    </Grid>

                    <Grid container >
                        <Grid item xs={12} style={{ textAlign: "right" }}>
                            {disable ? null : <Button onClick={onUpdateClick} style={{ fontWeight: "bold" }} variant="contained" color="success">Update</Button>}
                            <Button onClick={closeModal} className="mx-3" style={{ fontWeight: "bold", backgroundColor: "grey" }} variant="contained">Cancel</Button>
                        </Grid>
                    </Grid>
                </Box>
            </Modal>
            <Snackbar open={openSuccess} autoHideDuration={6000} onClose={closeSuccess}>
                <Alert onClose={closeSuccess} severity="success" sx={{ width: '100%' }}>
                    Update customer successful!
                </Alert>
            </Snackbar>

            <Modal open={openDelete} onClose={closeDelete}>
                <Box sx={modalDelete}>
                    <Typography variant="h6" style={{ marginBottom: "20px" }}>Are you sure you want to delete this customer?</Typography>
                    <Grid item xs={12}>
                        <Button onClick={onDelete} style={{ backgroundColor: "red", marginRight: "20px" }} variant="contained">Delete</Button>
                        <Button onClick={closeDelete} style={{ backgroundColor: "grey" }} variant="contained">Cancel</Button>
                    </Grid>
                </Box>
            </Modal>
        </div>
    )
}

export default CustomerTable