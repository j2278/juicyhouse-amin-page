import CustomerTable from "./customer table/CustomerTable";
import AddCustomer from "./add customer/AddCustomer";
import { Grid, TextField, Pagination } from "@mui/material"
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios"

function filterCustomer(list, search) {
        let result = [];
        if (search === "") {
            result = list;
        } else {
            result = list.filter(element => element.fullname.includes(search) || element.phone.includes(search) || element.email.includes(search))
        }
        return result;
}

function CustomerComponent() {
    const dispatch = useDispatch();
    const crud = useSelector(state => state.crud.crud);
    const [customerInPage, setCustomerInPage] = useState([])
    const [search, setSearch] = useState("");
    const limit = 20;
    const [page, setPage] = useState(1);
    const [pageNo, setPageNo] = useState(0)

    const onSearchChange = (event) => {
        setSearch(event.target.value)
    }

    const onPageChange = (event, value) => {
        setPage(value)
    }

    const getAllCustomer = async() => {
        const response = await axios(`http://localhost:8000/customers`);
        const data = response.data;
        return data;
    }

    useEffect(() => {
        getAllCustomer()
            .then((data) => {
                dispatch({ type: "SET_ALL_CUSTOMER", payload: data.customerList })
                let filterCustomerList = filterCustomer(data.customerList, search);
                setCustomerInPage(filterCustomerList.slice(limit * (page - 1), limit * page))
                setPageNo(Math.ceil(filterCustomerList.length / limit))
            })
    }, [crud])

    useEffect(() => {
        getAllCustomer()
            .then((data) => {
                console.log(data.customerList)
                let filterCustomerList = filterCustomer(data.customerList, search);
                setCustomerInPage(filterCustomerList.slice(limit * (page - 1), limit * page))
                setPageNo(Math.ceil(filterCustomerList.length / limit))
                window.scrollTo(0, 5)
            })
    }, [search, page])

    useEffect(() => {
        setPage(1)
    }, [search])

    return (
        <div>
            <h1 className="text-center">List of Customers </h1>
            <Grid container>
                <Grid item xs={4}>
                    <AddCustomer></AddCustomer>
                </Grid>
                <Grid item xs={8} paddingTop="15px">
                    <TextField onChange={onSearchChange} variant="outlined" fullWidth size="small" label="Search customer"></TextField>
                </Grid>
            </Grid>
            <CustomerTable customerInPage={customerInPage} limit={limit} page={page}></CustomerTable>
            <Pagination sx={{ float: "right" }} className="my-3" page={page} count={pageNo} onChange={onPageChange} />
        </div>
    )
}

export default CustomerComponent