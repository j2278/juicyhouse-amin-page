import { Button, Modal, Box, Typography, Grid, TextField, Alert, Snackbar } from "@mui/material"
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import { useState } from "react";
import axios from "axios";
import { useSelector, useDispatch } from "react-redux";

const modalStyle = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 1100,
    height: 350,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const inputStyle = {
    paddingRight: "20px"
}

function AddCustomer() {
    const dispatch = useDispatch();
    const crud = useSelector(state => state.crud.crud);
    const allCustomer = useSelector(state => state.customer.allCustomer)
    const [openModal, setOpenModal] = useState(false);
    const [errorDisplay, setErrorDisplay] = useState("none");
    const [openSuccess, setOpenSuccess] = useState(false);
    const [isValid, setIsValid] = useState(false);

    const [customerInput, setCustomerInput] = useState({
        fullname: "",
        email: "",
        phone: "",
        address: "",
        city: "",
        country: "",
    });

    const [error, setError] = useState({
        fullname: "Full name is empty!",
        email: "Email is empty!",
        phone: "Phone number is empty!",
        address: "Address is empty!",
        city: "City is empty!",
        country: "Country is empty!",
    })

    const closeModal = () => {
        setOpenModal(false);
        setErrorDisplay("none")
        setCustomerInput({
            fullname: "",
            email: "",
            phone: "",
            address: "",
            city: "",
            country: "",
        })
        setError({
            fullname: "Full name is empty!",
            email: "Email is empty!",
            phone: "Phone number is empty!",
            address: "Address is empty!",
            city: "City is empty!",
            country: "Country is empty!",
        })
        setIsValid(false)
    }

    const closeSuccess = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpenSuccess(false);
    };

    const onNameChange = (event) => {
        setCustomerInput({ ...customerInput, fullname: event.target.value })

        if (event.target.value === "") {
            setError({ ...error, fullname: "Full name is empty!" })
            setIsValid(false)
        } else {
            setError({ ...error, fullname: "none" })
            setIsValid(true)
        }
    }

    const onEmailChange = (event) => {
        setCustomerInput({ ...customerInput, email: event.target.value })
        let emailListExist = allCustomer.filter(element => element.email === event.target.value)

        if (event.target.value === "") {
            setError({ ...error, email: "Email is empty!" });
            setIsValid(false)
        } else if (/\S+@\S+\.\S+/.test(event.target.value) === false) {
            setError({ ...error, email: "Email syntax is not valid!" });
            setIsValid(false)
        } else if (emailListExist.length > 0) {
            setError({ ...error, email: "This email is already exist!" })
            setIsValid(false)
        } else {
            setError({ ...error, email: "none" });
            setIsValid(true)
        }
    }

    const onPhoneChange = (event) => {
        setCustomerInput({ ...customerInput, phone: event.target.value })
        let phonelListExist = allCustomer.filter(element => element.phone === event.target.value)

        if (event.target.value === "") {
            setError({ ...error, phone: "Phone number is empty!" })
            setIsValid(false)
        } else if (isNaN(event.target.value) || event.target.value.length !== 10) {
            setError({ ...error, phone: "Phone number must be a 10 digit number!" })
            setIsValid(false)
        } else if (phonelListExist.length > 0) {
            setError({ ...error, phone: "This phone number is already exist!" })
            setIsValid(false)
        } else {
            setError({ ...error, phone: "none" })
            setIsValid(true)
        }
    }

    const onAddressChange = (event) => {
        setCustomerInput({ ...customerInput, address: event.target.value })

        if (event.target.value === "") {
            setError({ ...error, address: "Address is empty!" });
            setIsValid(false)
        } else {
            setError({ ...error, address: "none" });
            setIsValid(true)
        }
    }

    const onCityChange = (event) => {
        setCustomerInput({ ...customerInput, city: event.target.value })

        if (event.target.value === "") {
            setError({ ...error, city: "City is empty!" });
            setIsValid(false)
        } else {
            setError({ ...error, city: "none" });
            setIsValid(true)
        }
    }

    const onCountryChange = (event) => {
        setCustomerInput({ ...customerInput, country: event.target.value })

        if (event.target.value === "") {
            setError({ ...error, country: "Country origin is empty!" });
            setIsValid(false)
        } else {
            setError({ ...error, country: "none" });
            setIsValid(true)
        }
    }


    const createNewCustomer = async () => {
        const response = await axios(`http://localhost:8000/customers`, {
            method: `POST`,
            data: customerInput,
            header: { "Content-Type": "application/json;charset=UTF-8" }
        })
        const data = response.data;
        return data;
    }

    const onInsertClick = () => {
        setErrorDisplay("block")
        if (isValid) {
            setOpenSuccess(true)
            closeModal()
            createNewCustomer()
                .then((data) => {
                    dispatch({ type: "RELOAD_ALL_DATA", payload: crud + 1 })
                })
        }
    }

    return (
        <div>
            <Button onClick={() => setOpenModal(true)} className="my-3" color="success" variant="contained"> <AddCircleOutlineIcon style={{ marginRight: "5px" }} />Add new customer</Button>
            <Modal open={openModal} onClose={closeModal}>
                <Box sx={modalStyle}>
                    <Typography className="text-center" variant="h6">
                        Insert New Customer
                    </Typography>
                    <Grid container className="my-4">
                        <Grid item xs={4}>
                            <TextField onChange={onNameChange} style={inputStyle} size="small" fullWidth label="Full name" variant="outlined"></TextField>
                            {error.fullname === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.fullname}</Typography>}
                        </Grid>
                        <Grid item xs={4}>
                            <TextField onChange={onEmailChange} style={inputStyle} size="small" fullWidth label="Email" variant="outlined"></TextField>
                            {error.email === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.email}</Typography>}
                        </Grid>
                        <Grid item xs={4}>
                            <TextField onChange={onPhoneChange} style={inputStyle} size="small" fullWidth label="Phone" variant="outlined"></TextField>
                            {error.phone === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.phone}</Typography>}
                        </Grid>
                    </Grid>
                    <Grid container className="my-4">
                        <Grid item xs={4}>
                            <TextField onChange={onAddressChange} style={inputStyle} size="small" fullWidth label="Address" variant="outlined"></TextField>
                            {error.address === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.address}</Typography>}
                        </Grid>
                        <Grid item xs={4}>
                            <TextField onChange={onCityChange} style={inputStyle} size="small" fullWidth label="City" variant="outlined"></TextField>
                            {error.city === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.city}</Typography>}
                        </Grid>
                        <Grid item xs={4}>
                            <TextField onChange={onCountryChange} style={inputStyle} size="small" fullWidth label="Country" variant="outlined"></TextField>
                            {error.country === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.country}</Typography>}
                        </Grid>
                    </Grid>
                    <Grid container >
                        <Grid item xs={12} style={{ textAlign: "right" }}>
                            <Button onClick={onInsertClick} style={{ fontWeight: "bold" }} variant="contained" color="success">Insert</Button>
                            <Button onClick={closeModal} className="mx-3" style={{ fontWeight: "bold", backgroundColor: "grey" }} variant="contained">Cancel</Button>
                        </Grid>
                    </Grid>
                </Box>
            </Modal>
            <Snackbar open={openSuccess} autoHideDuration={6000} onClose={closeSuccess}>
                <Alert onClose={closeSuccess} severity="success" sx={{ width: '100%' }}>
                    Insert new product successful!
                </Alert>
            </Snackbar>
        </div>
    )
}

export default AddCustomer