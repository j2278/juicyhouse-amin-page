import ProductTable from "../product/product table/ProductTable";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import AddProduct from "./add product/AddProduct";
import { Grid, TextField, Pagination } from "@mui/material"
import axios from "axios"

function filterProduct(list, search) {
    let result = [];
    if (search === "") {
        result = list;
    } else {
        result = list.filter(element => element.keyword.toLowerCase().includes(search.toLowerCase()))
    }
    return result;
}

function ProductComponent() {
    const dispatch = useDispatch();
    const crud = useSelector(state => state.crud.crud);
    const [productInPage, setProductInPage] = useState([])
    const [search, setSearch] = useState("");
    const limit = 20;
    const [page, setPage] = useState(1);
    const [pageNo, setPageNo] = useState(0)

    const onSearchChange = (event) => {
        setSearch(event.target.value)
    }

    const onPageChange = (event, value) => {
        setPage(value)
    }

    const getAllProduct = async () => {
        const response = await axios(`http://localhost:8000/products`);
        const data = response.data;
        return data;
    }

    useEffect(() => {
        getAllProduct()
            .then((data) => {
                dispatch({ type: "SET_ALL_PRODUCT", payload: data.productList })
                let filterProductList = filterProduct(data.productList, search);
                setProductInPage(filterProductList.slice(limit * (page - 1), limit * page))
                setPageNo(Math.ceil(filterProductList.length / limit))
            })
    }, [crud])

    useEffect(() => {
        getAllProduct()
            .then((data) => {
                dispatch({ type: "SET_ALL_PRODUCT", payload: data.productList })
                let filterProductList = filterProduct(data.productList, search);
                setProductInPage(filterProductList.slice(limit * (page - 1), limit * page))
                setPageNo(Math.ceil(filterProductList.length / limit))
                window.scrollTo(0, 5)
            })
    }, [search, page])

    useEffect(() => {
        setPage(1)
    }, [search])

    return (
        <div>
            <h1 className="text-center">List of Products </h1>
            <Grid container>
                <Grid item xs={4}>
                    <AddProduct></AddProduct>
                </Grid>
                <Grid item xs={8} sx={{ paddingTop: "15px" }}>
                    <TextField onChange={onSearchChange} variant="outlined" label="Search product" size="small" fullWidth />
                </Grid>
            </Grid>
            <ProductTable productInPage={productInPage} limit={limit} page={page}></ProductTable>
            <Pagination sx={{ float: "right" }} className="my-3" page={page} count={pageNo} onChange={onPageChange} />
        </div>
    )
}

export default ProductComponent