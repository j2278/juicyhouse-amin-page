import { Tooltip, Table, TableCell, TableRow, TableHead, TableBody, Button, Modal, Box, Typography, Grid, TextField, TextareaAutosize, Alert, Snackbar, MenuItem, Menu } from "@mui/material"
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import axios from "axios"
import VisibilityIcon from '@mui/icons-material/Visibility';
import SettingsIcon from '@mui/icons-material/Settings';

const modalStyle = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 1100,
    height: 650,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const modalDelete = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    height: 200,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const inputStyle = {
    paddingRight: "20px"
}

function ProductTable({ productInPage, limit, page }) {
    const dispatch = useDispatch()
    const typeList = useSelector(state => state.product.typeList)
    const flavorList = useSelector(state => state.product.flavorList)
    const capacityList = useSelector(state => state.product.capacityList)
    const originList = useSelector(state => state.product.originList)
    const allProduct = useSelector(state => state.product.allProduct);
    const crud = useSelector(state => state.crud.crud);
    const [openModal, setOpenModal] = useState(false);
    const [openDelete, setOpenDelete] = useState(false);
    const [errorDisplay, setErrorDisplay] = useState("none");
    const [isValid, setIsValid] = useState(false)
    const [openAlert, setOpenAlert] = useState(false);
    const [openSuccess, setOpenSuccess] = useState(false);
    const [disable, setDisable] = useState(true)
    const [anchorEl, setAnchorEl] = useState(null);
    const openMenu = Boolean(anchorEl);

    const onSettingClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const closeMenu = () => {
        setAnchorEl(null);
    };

    const [productInput, setProductInput] = useState({})

    const [error, setError] = useState({
        name: "none",
        brand: "none",
        type: "none",
        capacity: "none",
        flavor: "none",
        productOrigin: "none",
        imageUrl: "none",
        buyPrice: "none",
        promotionPrice: "none",
        description: "none"
    })

    const onNameChange = (event) => {
        setProductInput({ ...productInput, name: event.target.value })

        if (event.target.value === "") {
            setError({ ...error, name: "Product name is empty!" })
            setIsValid(false);
        } else {
            setError({ ...error, name: "none" })
            setIsValid(true)
        }
    }

    const onBrandChange = (event) => {
        setProductInput({ ...productInput, brand: event.target.value })

        if (event.target.value === "") {
            setError({ ...error, brand: "Product brand is empty!" })
            setIsValid(false)
        } else {
            setError({ ...error, brand: "none" })
            setIsValid(true)
        }
    }

    const onTypeChange = (event) => {
        setProductInput({ ...productInput, type: event.target.value })

        if (event.target.value === "NONE") {
            setError({ ...error, type: "Product type is empty!" })
            setIsValid(false)
        } else {
            setError({ ...error, type: "none" })
            setIsValid(true)
        }
    }

    const onCapacityChange = (event) => {
        setProductInput({ ...productInput, capacity: event.target.value })

        if (event.target.value === 0) {
            setError({ ...error, capacity: "Product capacity is empty!" })
            setIsValid(false)
        } else {
            setError({ ...error, capacity: "none" })
            setIsValid(true)
        }
    }

    const onFlavorChange = (event) => {
        setProductInput({ ...productInput, flavor: event.target.value })

        if (event.target.value === "NONE") {
            setError({ ...error, flavor: "Product flavor is empty!" })
            setIsValid(false)
        } else {
            setError({ ...error, flavor: "none" })
            setIsValid(true)
        }
    }

    const onOriginChange = (event) => {
        setProductInput({ ...productInput, productOrigin: event.target.value })

        if (event.target.value === "NONE") {
            setError({ ...error, origin: "Product origin is empty!" })
            setIsValid(false)
        } else {
            setError({ ...error, origin: "none" })
            setIsValid(true)
        }
    }

    const onImageUrlChange = (event) => {
        setProductInput({ ...productInput, imageUrl: event.target.value })

        let isImageExist = allProduct.filter(element => element.imageUrl === event.target.value)
        if (event.target.value === "") {
            setError({ ...error, imageUrl: "Product image URL is empty!" })
            setIsValid(false)
        } else if (isImageExist.length > 0) {
            setError({ ...error, imageUrl: "Product image URL is already exist!" })
            setIsValid(false)
        } else {
            setError({ ...error, imageUrl: "none" })
            setIsValid(true)
        }
    }

    const onBuyPriceChange = (event) => {
        setProductInput({ ...productInput, buyPrice: event.target.value })

        if (event.target.value === "") {
            setError({ ...error, buyPrice: "Product buy price is empty!" })
            setIsValid(false)
        } else if (isNaN(event.target.value)) {
            setError({ ...error, buyPrice: "Product buy price must be a number!" })
            setIsValid(false)
        } else {
            setError({ ...error, buyPrice: "none" })
            setIsValid(true)
        }
    }

    const onPromotionPriceChange = (event) => {
        setProductInput({ ...productInput, promotionPrice: event.target.value })

        if (event.target.value === "") {
            setError({ ...error, promotionPrice: "Product promotion price is empty!" })
            setIsValid(false)
        } else if (isNaN(event.target.value)) {
            setError({ ...error, promotionPrice: "Product promotion must be a number!" })
            setIsValid(false)
        } else {
            setError({ ...error, promotionPrice: "none" })
            setIsValid(true)

        }
    }

    const onDescriptionChange = (event) => {
        setProductInput({ ...productInput, description: event.target.value })

        if (event.target.value === "") {
            setError({ ...error, description: "Product description is empty!" })
            setIsValid(false)
        } else {
            setError({ ...error, description: "none" })
            setIsValid(true)
        }
    }

    const onLastestChange = (event) => {
        setProductInput({ ...productInput, lastest: event.target.value })
    }

    const onBestSaleChange = (event) => {
        setProductInput({ ...productInput, bestSale: event.target.value })
    }

    const closeModal = () => {
        setOpenModal(false);
        setErrorDisplay("none");
        setDisable(true)
        setError({
            name: "none",
            brand: "none",
            type: "none",
            capacity: "none",
            flavor: "none",
            productOrigin: "none",
            imageUrl: "none",
            buyPrice: "none",
            promotionPrice: "none",
            description: "none"
        })
    }

    const closeAlert = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpenAlert(false);
    };

    const closeSuccess = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpenSuccess(false);
    };

    const onViewClick = (element) => {
        setOpenModal(true);
        setProductInput(element)
        setIsValid(true)
    }

    const onEditClick = () => {
        setDisable(false);
        closeMenu()
    }

    const onEraseClick = () => {
        setOpenDelete(true);
        closeMenu()
    }

    const closeDelete = () => {
        setOpenDelete(false);
    }

    const deleteProduct = async () => {
        const response = await axios(`http://localhost:8000/products/${productInput._id}`, {
            method: `DELETE`
        });
        const data = response.data;
        return data;
    }

    const updateProduct = async () => {
        let date = new Date();
        let today = `${date.getHours()}:${date.getMinutes()} ${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`
        let body = {
            name: productInput.name,
            brand: productInput.brand,
            type: productInput.type,
            capacity: productInput.capacity,
            flavor: productInput.flavor,
            productOrigin: productInput.productOrigin,
            imageUrl: productInput.imageUrl,
            buyPrice: productInput.buyPrice,
            promotionPrice: productInput.promotionPrice,
            description: productInput.description,
            lastest: productInput.lastest,
            bestSale: productInput.bestSale,
            timeUpdated: today,
        }
        const response = await axios(`http://localhost:8000/products/${productInput._id}`, {
            method: `PUT`,
            data: body,
            header: { "Content-Type": "application/json;charset=UTF-8" }
        })
        const data = response.data;
        return data;
    }

    const onUpdateClick = () => {
        console.log("Click")
        setErrorDisplay("block")
        if (isValid) {
            let isExist = allProduct.filter(element => element.name === productInput.name && element.brand === productInput.brand &&
                element.type === productInput.type && element.capacity === productInput.capacity && element.flavor === productInput.flavor &&
                element.productOrigin === productInput.productOrigin && element._id !== productInput._id)
            if (isExist.length > 0) {
                setOpenAlert(true)
            } else {
                setOpenSuccess(true)
                closeModal()
                updateProduct()
                    .then((data) => {
                        dispatch({ type: "RELOAD_ALL_DATA", payload: crud + 1 })
                    })
            }
        }

    }

    const onDelete = () => {
        deleteProduct()
            .then((data) => {
                setOpenSuccess(true);
                closeModal();
                closeDelete();
                dispatch({ type: "RELOAD_ALL_DATA", payload: crud + 1 })
            })
    }
    return (
        <div>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell style={{ fontWeight: "bold" }} >No</TableCell>
                        <TableCell style={{ fontWeight: "bold" }}>Image</TableCell>
                        <TableCell style={{ fontWeight: "bold" }}>Brand</TableCell>
                        <TableCell style={{ fontWeight: "bold" }}>Name</TableCell>
                        <TableCell style={{ fontWeight: "bold" }}>Type</TableCell>
                        <TableCell style={{ fontWeight: "bold" }}>Capacity</TableCell>
                        <TableCell style={{ fontWeight: "bold" }}>Flavor</TableCell>
                        <TableCell style={{ fontWeight: "bold" }}>Origin</TableCell>
                        <TableCell style={{ fontWeight: "bold" }}>Promotion Price</TableCell>
                        <TableCell style={{ fontWeight: "bold" }}>View</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {productInPage.map((element, index) => {
                        return <TableRow key={index}>
                            <TableCell>{(index + 1) + (limit * (page - 1))}</TableCell>
                            <TableCell> <img alt="" src={element.imageUrl} style={{ width: "50px", height: "50px" }}></img> </TableCell>
                            <TableCell>{element.brand}</TableCell>
                            <TableCell>{element.name}</TableCell>
                            <TableCell>{element.type}</TableCell>
                            <TableCell>{element.capacity}ML</TableCell>
                            <TableCell>{element.flavor}</TableCell>
                            <TableCell>{element.productOrigin}</TableCell>
                            <TableCell>{element.promotionPrice.toLocaleString()}</TableCell>
                            <TableCell>
                                <VisibilityIcon onClick={() => onViewClick(element)} style={{ color: "black", marginRight: "5px" }} />
                            </TableCell>
                        </TableRow>
                    })}
                </TableBody>
            </Table>
            <Modal open={openModal} onClose={closeModal}>
                <Box sx={modalStyle}>
                    <Grid container>
                        <Grid item xs={8} style={{ textAlign: "right", paddingRight: "30px" }}>
                            <Typography fontWeight="bold" variant="h6"> PRODUCT INFORMATION</Typography>
                        </Grid>
                        <Grid item xs={4} style={{ textAlign: "right" }}>
                            <Tooltip title="Setting"><SettingsIcon onClick={onSettingClick} /></Tooltip>
                            <Menu anchorEl={anchorEl} open={openMenu} onClose={closeMenu}>
                                <MenuItem onClick={onEditClick}> <EditIcon className="mx-2" /> Edit </MenuItem>
                                <MenuItem onClick={onEraseClick}> <DeleteIcon className="mx-2" /> Delete </MenuItem>
                            </Menu>
                        </Grid>
                    </Grid>
                    <Grid container className="my-4">
                        <Grid item xs={4}>
                            <TextField disabled={disable} value={productInput.name} onChange={onNameChange} style={inputStyle} size="small" fullWidth label="Name" variant="outlined"></TextField>
                            {error.name === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.name}</Typography>}
                        </Grid>
                        <Grid item xs={4}>
                            <TextField disabled={disable} value={productInput.brand} onChange={onBrandChange} style={inputStyle} size="small" fullWidth label="Brand" variant="outlined"></TextField>
                            {error.brand === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.brand}</Typography>}
                        </Grid>
                        <Grid item xs={4} style={{ paddingRight: "20px" }}>
                            <TextField disabled={disable} select onChange={onTypeChange} value={productInput.type} size="small" fullWidth label="Type" variant="outlined">
                                {typeList.map((element, index) => {
                                    return <MenuItem key={index} value={element.typeId}>{element.typeName}</MenuItem>
                                })}
                            </TextField>
                            {error.type === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.type}</Typography>}
                        </Grid>
                    </Grid>
                    <Grid container className="my-4">
                        <Grid item xs={4}>
                            <TextField disabled={disable} select value={productInput.capacity} onChange={onCapacityChange} style={inputStyle} size="small" fullWidth label="Capacity" variant="outlined">
                                {capacityList.map((element, index) => {
                                    return <MenuItem key={index} value={element.capacityId}>{element.capacityMl}</MenuItem>
                                })}
                            </TextField>
                            {error.capacity === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.capacity}</Typography>}
                        </Grid>
                        <Grid item xs={4}>
                            <TextField disabled={disable} select onChange={onFlavorChange} value={productInput.flavor} style={inputStyle} size="small" fullWidth label="Flavor" variant="outlined">
                                {flavorList.map((element, index) => {
                                    return <MenuItem key={index} value={element.flavorId}>{element.flavorName}</MenuItem>
                                })}
                            </TextField>
                            {error.flavor === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.flavor}</Typography>}
                        </Grid>
                        <Grid item xs={4}>
                            <TextField disabled={disable} select value={productInput.productOrigin} onChange={onOriginChange} style={inputStyle} size="small" fullWidth label="Origin" variant="outlined">
                                {originList.map((element, index) => {
                                    return <MenuItem key={index} value={element.originId}>{element.originName}</MenuItem>
                                })}
                            </TextField>
                            {error.origin === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.origin}</Typography>}
                        </Grid>
                    </Grid>
                    <Grid container className="my-4">
                        <Grid item xs={4}>
                            <TextField disabled={disable} value={productInput.imageUrl} onChange={onImageUrlChange} style={inputStyle} size="small" fullWidth label="Image Url" variant="outlined"></TextField>
                            {error.imageUrl === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.imageUrl}</Typography>}
                        </Grid>
                        <Grid item xs={4}>
                            <TextField disabled={disable} value={productInput.buyPrice} onChange={onBuyPriceChange} style={inputStyle} size="small" fullWidth label="Buy Price" variant="outlined"></TextField>
                            {error.buyPrice === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.buyPrice}</Typography>}
                        </Grid>
                        <Grid item xs={4}>
                            <TextField disabled={disable} value={productInput.promotionPrice} onChange={onPromotionPriceChange} style={inputStyle} size="small" fullWidth label="Promotion Price" variant="outlined"></TextField>
                            {error.promotionPrice === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.promotionPrice}</Typography>}
                        </Grid>
                    </Grid>
                    <Grid container className="my-3">
                        <Grid item xs={12} style={{ paddingRight: "20px" }}>
                            <TextareaAutosize disabled={disable} value={productInput.description} onChange={onDescriptionChange} style={{ width: "100%" }} minRows={5} placeholder="Description"></TextareaAutosize>
                            {error.description === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.description}</Typography>}
                        </Grid>
                    </Grid>
                    <Grid container marginBottom="20px">
                        <Grid item xs={3}>
                            <TextField select disabled={disable} value={productInput.lastest} onChange={onLastestChange} style={inputStyle} size="small" fullWidth label="Lastest product" variant="outlined">
                                <MenuItem value={true}>True</MenuItem>
                                <MenuItem value={false}>False</MenuItem>
                            </TextField>
                        </Grid>
                        <Grid item xs={3}>
                            <TextField select disabled={disable} value={productInput.bestSale} onChange={onBestSaleChange} style={inputStyle} size="small" fullWidth label="Best sale" variant="outlined">
                                <MenuItem value={true}>True</MenuItem>
                                <MenuItem value={false}>False</MenuItem>
                            </TextField>
                        </Grid>
                        <Grid item xs={3}>
                            <TextField disabled value={productInput.timeCreated} style={inputStyle} size="small" fullWidth label="Time created" variant="outlined"></TextField>
                        </Grid>
                        <Grid item xs={3}>
                            <TextField disabled value={productInput.timeUpdated} style={inputStyle} size="small" fullWidth label="Time updated" variant="outlined"></TextField>
                        </Grid>
                    </Grid>
                    <Grid container >
                        <Grid item xs={12} style={{ textAlign: "right" }}>
                            {disable ? null : <Button onClick={onUpdateClick} style={{ fontWeight: "bold" }} variant="contained" color="success">Update</Button>}
                            <Button onClick={closeModal} className="mx-3" style={{ fontWeight: "bold", backgroundColor: "grey" }} variant="contained">Cancel</Button>
                        </Grid>
                    </Grid>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={closeAlert}>
                <Alert onClose={closeAlert} severity="warning" sx={{ width: '100%' }}>
                    This product is already exist!
                </Alert>
            </Snackbar>
            <Snackbar open={openSuccess} autoHideDuration={6000} onClose={closeSuccess}>
                <Alert onClose={closeSuccess} severity="success" sx={{ width: '100%' }}>
                    Update product successful!
                </Alert>
            </Snackbar>
            <Modal open={openDelete} onClose={closeDelete}>
                <Box sx={modalDelete}>
                    <Typography variant="h6" style={{ marginBottom: "20px" }}>Are you sure you want to delete this product?</Typography>
                    <Grid item xs={12}>
                        <Button onClick={onDelete} style={{ backgroundColor: "red", marginRight: "20px" }} variant="contained">Delete</Button>
                        <Button onClick={closeDelete} style={{ backgroundColor: "grey" }} variant="contained">Cancel</Button>
                    </Grid>
                </Box>
            </Modal>

        </div>

    )
}

export default ProductTable