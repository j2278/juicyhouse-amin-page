import { Button, Modal, Box, Typography, Grid, TextField, TextareaAutosize, Alert, Snackbar, MenuItem } from "@mui/material"
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import { useState } from "react";
import axios from "axios";
import { useSelector, useDispatch } from "react-redux";

const modalStyle = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 1000,
    height: 600,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const inputStyle = {
    paddingRight: "20px"
}

function AddProduct() {
    const typeList = useSelector(state => state.product.typeList)
    const flavorList = useSelector(state => state.product.flavorList)
    const capacityList = useSelector(state => state.product.capacityList)
    const originList = useSelector(state => state.product.originList)
    const dispatch = useDispatch();
    const crud = useSelector(state => state.crud.crud);
    const allProduct = useSelector(state => state.product.allProduct)
    const [openModal, setOpenModal] = useState(false);
    const [errorDisplay, setErrorDisplay] = useState("none");
    const [openAlert, setOpenAlert] = useState(false);
    const [openSuccess, setOpenSuccess] = useState(false);
    const [isValid, setIsValid] = useState(false);

    const [productInput, setProductInput] = useState({
        name: "",
        brand: "",
        type: "NONE",
        capacity: 0,
        flavor: "NONE",
        productOrigin: "NONE",
        imageUrl: "",
        buyPrice: 0,
        promotionPrice: 0,
        description: ""
    });

    const [error, setError] = useState({
        name: "Product name is empty!",
        brand: "Product brand is empty!",
        type: "Product type is empty!",
        capacity: "Product capacity is empty!",
        flavor: "Product flavor is empty!",
        productOrigin: "Product origin is empty!",
        imageUrl: "Product image URL is empty!",
        buyPrice: "Product buy price is empty!",
        promotionPrice: "Product promotion price is empty!",
        description: "Product description is empty!"
    })

    const closeModal = () => {
        setOpenModal(false);
        setErrorDisplay("none")
        setProductInput({
            name: "",
            brand: "",
            type: "NONE",
            capacity: 0,
            flavor: "NONE",
            productOrigin: "NONE",
            imageUrl: "",
            buyPrice: 0,
            promotionPrice: 0,
            description: ""
        })
        setError({
            name: "Product name is empty!",
            brand: "Product brand is empty!",
            type: "Product type is empty!",
            capacity: "Product capacity is empty!",
            flavor: "Product flavor is empty!",
            origin: "Product origin is empty!",
            imageUrl: "Product image URL is empty!",
            buyPrice: "Product buy price is empty!",
            promotionPrice: "Product promotion price is empty!",
            description: "Product description is empty!"
        })
        setIsValid(false)
    }

    const closeAlert = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setOpenAlert(false);
      };

      const closeSuccess = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setOpenSuccess(false);
      };

    const onNameChange = (event) => {
        setProductInput({ ...productInput, name: event.target.value })

        if (event.target.value === "") {
            setError({ ...error, name: "Product name is empty!" })
            setIsValid(false)
        } else {
            setError({ ...error, name: "none" })
            setIsValid(true)
        }
    }

    const onBrandChange = (event) => {
        setProductInput({ ...productInput, brand: event.target.value })

        if (event.target.value === "") {
            setError({ ...error, brand: "Product brand is empty!" });
            setIsValid(false)
        } else {
            setError({ ...error, brand: "none" });
            setIsValid(true)
        }
    }

    const onTypeChange = (event) => {
        setProductInput({ ...productInput, type: event.target.value })

        if (event.target.value === "NONE") {
            setError({ ...error, type: "Product type is empty!" })
            setIsValid(false)
        } else {
            setError({ ...error, type: "none" });
            setIsValid(true)
        }
    }

    const onCapacityChange = (event) => {
        setProductInput({ ...productInput, capacity: event.target.value })

        if (event.target.value === 0) {
            setError({ ...error, capacity: "Product capacity is empty!" });
            setIsValid(false)
        } else {
            setError({ ...error, capacity: "none" });
            setIsValid(true)
        }
    }

    const onFlavorChange = (event) => {
        setProductInput({ ...productInput, flavor: event.target.value })

        if (event.target.value === "NONE") {
            setError({ ...error, flavor: "Product flavor is empty!" });
            setIsValid(false)
        } else {
            setError({ ...error, flavor: "none" });
            setIsValid(true)
        }
    }

    const onOriginChange = (event) => {
        setProductInput({ ...productInput, productOrigin: event.target.value })

        if (event.target.value === "NONE") {
            setError({ ...error, origin: "Product origin is empty!" });
            setIsValid(false)
        } else {
            setError({ ...error, origin: "none" });
            setIsValid(true)
        }
    }

    const onImageUrlChange = (event) => {
        setProductInput({ ...productInput, imageUrl: event.target.value })

        let isImageExist = allProduct.filter(element => element.imageUrl === event.target.value)
        if (event.target.value === "") {
            setError({ ...error, imageUrl: "Product image URL is empty!" });
            setIsValid(false)
        } else if (isImageExist.length > 0){
            setError({ ...error, imageUrl: "Product image URL is already exist!" });
            setIsValid(false)
        } else {
            setError({ ...error, imageUrl: "none" });
            setIsValid(true)
        }
    }

    const onBuyPriceChange = (event) => {
        setProductInput({ ...productInput, buyPrice: event.target.value })

        if (event.target.value === "") {
            setError({ ...error, buyPrice: "Product buy price is empty!" });
            setIsValid(false)
        } else if (isNaN(event.target.value)) {
            setError({ ...error, buyPrice: "Product buy price must be a number!" });
            setIsValid(false)
        } else {
            setError({ ...error, buyPrice: "none" });
            setIsValid(true)
        }
    }

    const onPromotionPriceChange = (event) => {
        setProductInput({ ...productInput, promotionPrice: event.target.value })

        if (event.target.value === "") {
            setError({ ...error, promotionPrice: "Product promotion price is empty!" });
            setIsValid(false)
        } else if (isNaN(event.target.value)) {
            setError({ ...error, promotionPrice: "Product promotion must be a number!" });
            setIsValid(false)
        } else {
            setError({ ...error, promotionPrice: "none" });
            setIsValid(true)
        }
    }

    const onDescriptionChange = (event) => {
        setProductInput({ ...productInput, description: event.target.value })

        if (event.target.value === "") {
            setError({ ...error, description: "Product description is empty!" });
            setIsValid(false)
        } else {
            setError({ ...error, description: "none" });
            setIsValid(true)
        }
    }

    const createNewProduct = async () => {
        const response = await axios(`http://localhost:8000/products`, {
            method: `POST`,
            data: productInput,
            header: { "Content-Type": "application/json;charset=UTF-8" }
        })
        const data = response.data;
        return data;
    }

    const onInsertClick = () => {
        setErrorDisplay("block")
        if (isValid) {
            let isExist = allProduct.filter(element => element.name === productInput.name && element.brand === productInput.brand &&
                element.type === productInput.type && element.capacity === productInput.capacity && element.flavor === productInput.flavor &&
                element.productOrigin === productInput.productOrigin)
            if (isExist.length > 0) {
                setOpenAlert(true)
            } else {
                setOpenSuccess(true)
                closeModal()
                createNewProduct()
                    .then((data) => {
                        dispatch({ type: "RELOAD_ALL_DATA", payload: crud + 1 })
                    })
            }
        }
    }

    return (
        <div>
            <Button onClick={() => setOpenModal(true)} className="my-3" color="success" variant="contained"> <AddCircleOutlineIcon style={{ marginRight: "5px" }} />Add new product</Button>
            <Modal open={openModal} onClose={closeModal}>
                <Box sx={modalStyle}>
                    <Typography className="text-center" variant="h6">
                        Insert New Product
                    </Typography>
                    <Grid container className="my-4">
                        <Grid item xs={4}>
                            <TextField onChange={onNameChange} style={inputStyle} size="small" fullWidth label="Name" variant="outlined"></TextField>
                            {error.name === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.name}</Typography>}
                        </Grid>
                        <Grid item xs={4}>
                            <TextField onChange={onBrandChange} style={inputStyle} size="small" fullWidth label="Brand" variant="outlined"></TextField>
                            {error.brand === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.brand}</Typography>}
                        </Grid>
                        <Grid item xs={4} style={{ paddingRight: "20px" }}>
                            <TextField select onChange={onTypeChange} value={productInput.type} size="small" fullWidth label="Type" variant="outlined">
                                {typeList.map((element, index) => {
                                    return <MenuItem key={index} value={element.typeId}>{element.typeName}</MenuItem>
                                })}
                            </TextField>
                            {error.type === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.type}</Typography>}
                        </Grid>
                    </Grid>
                    <Grid container className="my-4">
                        <Grid item xs={4}>
                            <TextField select value={productInput.capacity} onChange={onCapacityChange} style={inputStyle} size="small" fullWidth label="Capacity" variant="outlined">
                                {capacityList.map((element, index) => {
                                    return <MenuItem key={index} value={element.capacityId}>{element.capacityMl}</MenuItem>
                                })}
                            </TextField>
                            {error.capacity === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.capacity}</Typography>}
                        </Grid>
                        <Grid item xs={4}>
                            <TextField select onChange={onFlavorChange} value={productInput.flavor} style={inputStyle} size="small" fullWidth label="Flavor" variant="outlined">
                                {flavorList.map((element, index) => {
                                    return <MenuItem key={index} value={element.flavorId}>{element.flavorName}</MenuItem>
                                })}
                            </TextField>
                            {error.flavor === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.flavor}</Typography>}
                        </Grid>
                        <Grid item xs={4}>
                            <TextField select value={productInput.productOrigin} onChange={onOriginChange} style={inputStyle} size="small" fullWidth label="Origin" variant="outlined">
                                {originList.map((element, index) => {
                                    return <MenuItem key={index} value={element.originId}>{element.originName}</MenuItem>
                                })}
                            </TextField>
                            {error.origin === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.origin}</Typography>}
                        </Grid>
                    </Grid>
                    <Grid container className="my-4">
                        <Grid item xs={4}>
                            <TextField onChange={onImageUrlChange} style={inputStyle} size="small" fullWidth label="Image Url" variant="outlined"></TextField>
                            {error.imageUrl === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.imageUrl}</Typography>}
                        </Grid>
                        <Grid item xs={4}>
                            <TextField onChange={onBuyPriceChange} style={inputStyle} size="small" fullWidth label="Buy Price" variant="outlined"></TextField>
                            {error.buyPrice === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.buyPrice}</Typography>}
                        </Grid>
                        <Grid item xs={4}>
                            <TextField onChange={onPromotionPriceChange} style={inputStyle} size="small" fullWidth label="Promotion Price" variant="outlined"></TextField>
                            {error.promotionPrice === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.promotionPrice}</Typography>}
                        </Grid>
                    </Grid>
                    <Grid container className="my-4">
                        <Grid item xs={12} style={{ paddingRight: "20px" }}>
                            <TextareaAutosize onChange={onDescriptionChange} style={{ width: "100%" }} minRows={5} placeholder="Description"></TextareaAutosize>
                            {error.description === "none" ? null :
                                <Typography style={{ color: "red", display: errorDisplay }} variant="caption">{error.description}</Typography>}
                        </Grid>
                    </Grid>
                    <Grid container >
                        <Grid item xs={12} style={{ textAlign: "right" }}>
                            <Button onClick={onInsertClick} style={{ fontWeight: "bold" }} variant="contained" color="success">Insert</Button>
                            <Button onClick={closeModal} className="mx-3" style={{ fontWeight: "bold", backgroundColor: "grey" }} variant="contained">Cancel</Button>
                        </Grid>
                    </Grid>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={closeAlert}>
                <Alert onClose={closeAlert} severity="warning" sx={{ width: '100%' }}>
                    This product is already exist!
                </Alert>
            </Snackbar>
            <Snackbar open={openSuccess} autoHideDuration={6000} onClose={closeSuccess}>
                <Alert onClose={closeSuccess} severity="success" sx={{ width: '100%' }}>
                    Insert new product successful!
                </Alert>
            </Snackbar>
        </div>
    )
}

export default AddProduct