import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import VisibilityIcon from '@mui/icons-material/Visibility';
import { Tooltip, Table, TableCell, TableRow, TableHead, TableBody, Button, Modal, Box, Typography, Grid, TextField, Alert, Snackbar, MenuItem, Menu, Autocomplete, Stack } from "@mui/material"
import SettingsIcon from '@mui/icons-material/Settings';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import { useState, useEffect } from "react";
import axios from "axios";
import { useSelector, useDispatch } from "react-redux";
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';

const modalStyle = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 1100,
    height: 600,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const modalDeleteDetail = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    height: 200,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const inputStyle = {
    paddingRight: "20px"
}

function OrderTable({ orderInPage, page, limit }) {
    const allProduct = useSelector(state => state.product.allProduct)
    const allCustomer = useSelector(state => state.customer.allCustomer)
    const dispatch = useDispatch();
    const crud = useSelector(state => state.crud.crud);
    const [openModal, setOpenModal] = useState(false);
    const [openSuccess, setOpenSuccess] = useState(false);
    const [cart, setCart] = useState([])
    const [amount, setAmount] = useState();
    const [openAlertProduct, setOpenAlertProduct] = useState(false);
    const [total, setTotal] = useState(0)
    const [disable, setDisable] = useState(true)
    const [anchorEl, setAnchorEl] = useState(null);
    const openMenu = Boolean(anchorEl);
    const [orderSelected, setOrderSelected] = useState({ orderValue: 0 })
    const [customer, setCustomer] = useState({})
    const [orderDetails, setOrderDetails] = useState([])
    const [deleteList, setDeleteList] = useState([]);

    const statusList = [
        {statusName: "Delivering", statusId: 0},
        {statusName: "Devlired", statusId: 1}
    ]

    const [productError, setProductError] = useState({
        product: "You haven't chose the product yet!",
        amount: "You haven't chose the amount yet!"
    })


    const defaultProductValue = {
        name: "",
        brand: "",
        capacity: ""
    }
    const [productSelected, setProductSelected] = useState(defaultProductValue)

    const onStatusChange = (event) => {
        setOrderSelected({...orderSelected, orderStatus: event.target.value})
    }

    const onShipDateChange = (event) => {
        setOrderSelected({...orderSelected, shippedDate: event.target.value})
    }

    const onChangeProduct = (event, newvalue) => {
        if (newvalue === null) {
            setProductSelected(defaultProductValue);
            setProductError({ ...productError, product: "You haven't chose the product yet!" })
        } else {
            setProductSelected(newvalue);
            setProductError({ ...productError, product: "none" })
        }
    }

    const onAmountChange = (event) => {
        setAmount(event.target.value)
        if (event.target.value === "") {
            setProductError({ ...productError, amount: "You haven't chose the amount yet!" })
        } else if (isNaN(event.target.value)) {
            setProductError({ ...productError, amount: "The amount must be a number" })
        } else if (event.target.value < 1) {
            setProductError({ ...productError, amount: "The amount must be greater than 0" })
        } else {
            setProductError({ ...productError, amount: "none" })
        }
    }

    const onNoteChange = (event) => {
        setOrderSelected({...orderSelected, note: event.target.value})
    }

    const onAddClick = () => {
        if (productError.product === "none" && productError.amount === "none") {
            let obj = {
                product: productSelected._id,
                quantity: amount
            }
            let newCart = [...cart]
            newCart.push(obj)
            setCart(newCart)
            setTotal(total + (productSelected.promotionPrice * amount))
        } else {
            setOpenAlertProduct(true)
        }
    }

    const onRemoveCart = (paramCart) => {
        let newCart = cart.filter(element => element.product !== paramCart.product)
        setCart(newCart)
        let price = renderProduct(paramCart.product)[0].promotionPrice * paramCart.quantity
        setTotal(total - price)
    }

    const deleteOrderDetail = async(paramId) => {
        const response = await axios(`http://localhost:8000/orderdetails/${paramId}`, {
            method: "DELETE"
        })
        const data = response.data;
        return data;
    }

    const onRemoveDetail = (paramCart) => {
        let newOrderDetail = orderDetails.filter(element => element.product !== paramCart.product)
        setOrderDetails(newOrderDetail)
        let price = renderProduct(paramCart.product)[0].promotionPrice * paramCart.quantity
        setTotal(total - price)
        let list = [...deleteList]
        list.push(paramCart._id)
        setDeleteList(list)
    }

    const closeModal = () => {
        setOpenModal(false);
        setProductSelected(defaultProductValue);
        setCart([]);
        setDeleteList([])
        setOrderSelected({ orderValue: 0 })
        setTotal(0);
        setDisable(true)
        setProductError({
            product: "You haven't chose the product yet!",
            amount: "You haven't chose the amount yet!"
        })
    }

    const onSettingClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const closeMenu = () => {
        setAnchorEl(null);
    };

    const closeAlertProduct = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpenAlertProduct(false);
    };

    const closeSuccess = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpenSuccess(false);
    };

    const onViewClick = (element) => {
        setOpenModal(true);
        setOrderSelected(element)
    }

    const onEditClick = () => {
        setDisable(false);
        closeMenu()
    }


    function renderCustomer(paramCustomerId) {
        let result = {};
        let bI = 0;
        let isFound = false;
        while (!isFound && bI < allCustomer.length) {
            if (allCustomer[bI]._id === paramCustomerId) {
                result = allCustomer[bI];
                isFound = true;
            } else {
                bI++
            }
        }
        return result
    }

    const renderStatus = (paramStatus) => {
        let result = ""
        if (paramStatus === 0) {
            result = "Delivering"
        }
        if (paramStatus === 1) {
            result = "Delivered"
        }

        return result
    }

    const getCustomer = async () => {
        const response = await axios(`http://localhost:8000/customers/${orderSelected.customer}`)
        const data = response.data;
        return data;
    }

    const getOrderDetails = async () => {
        const response = await axios(`http://localhost:8000/orders/${orderSelected._id}/orderdetails`)
        const data = response.data;
        return data;
    }

    const updateOrder = async () => {
        let date = new Date();
        let today = `${date.getHours()}:${date.getMinutes()} ${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`
        let body = {
            orderValue: total,
            shippedDate: orderSelected.shippedDate,
            note: orderSelected.note,
            orderStatus: orderSelected.orderStatus,
            timeUpdated: today
        }

        const response = await axios(`http://localhost:8000/orders/${orderSelected._id}`, {
            method: `PUT`,
            data: body,
            header: { "Content-Type": "application/json;charset=UTF-8" }
        })
        const data = response.data;
        return data;
    }

    const createNewOrderDetail = async(cart) => {
        let body = {
            product : cart.product,
            quantity : cart.quantity,
            priceEach : renderProduct(cart.product)[0].promotionPrice
        }
        const response = await axios(`http://localhost:8000/orders/${orderSelected._id}/orderdetails/${cart.product}`, {
            method: `POST`,
            data: body,
            header: { "Content-Type": "application/json;charset=UTF-8" }
        })
        const data = response.data;
        return data;
    }

    const onUpdateClick = () => {
        setOpenSuccess(true)
        closeModal()
        updateOrder()
            .then((data) => {
                dispatch({ type: "RELOAD_ALL_DATA", payload: crud + 1 });
                for(let bI = 0; bI < cart.length; bI++){
                    createNewOrderDetail(cart[bI])
                    .then((data) => console.log(data))
                }
                for(let bI = 0; bI < deleteList.length; bI++){
                    console.log(deleteList[bI])
                    deleteOrderDetail(deleteList[bI])
                    .then((data) => {
                        console.log(`Delete order detail ${deleteList[bI]}`)
                    })
                }
            })
    }

    const renderProduct = (paramId) => {
        let result = allProduct.filter(element => element._id === paramId)
        return result
    }

    useEffect(() => {
        getCustomer()
            .then((data) => {
                setCustomer(data.customer)
            })
        setTotal(orderSelected.orderValue)
        getOrderDetails()
            .then((data) => {
                setOrderDetails(data.list)
            })
    }, [orderSelected])


    return (
        <div>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell style={{ fontWeight: "bold" }} >No</TableCell>
                        <TableCell style={{ fontWeight: "bold" }} >Created date</TableCell>
                        <TableCell style={{ fontWeight: "bold" }}>Value</TableCell>
                        <TableCell style={{ fontWeight: "bold" }}>Status</TableCell>
                        <TableCell style={{ fontWeight: "bold" }}>Shipped date</TableCell>
                        <TableCell style={{ fontWeight: "bold" }}>Customer name</TableCell>
                        <TableCell style={{ fontWeight: "bold" }}>Customer email</TableCell>
                        <TableCell style={{ fontWeight: "bold" }}>Customer phone</TableCell>
                        <TableCell style={{ fontWeight: "bold" }}>View</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {orderInPage.map((element, index) => {
                        return <TableRow key={index}>
                            <TableCell>{(index + 1) + (limit * (page - 1))}</TableCell>
                            <TableCell>{element.timeCreated}</TableCell>
                            <TableCell>{element.orderValue.toLocaleString()}</TableCell>
                            <TableCell>{renderStatus(element.orderStatus)}</TableCell>
                            <TableCell>{element.orderStatus === 0 ? "Not yet delivery" : element.shippedDate}</TableCell>
                            <TableCell>{renderCustomer(element.customer).fullname} </TableCell>
                            <TableCell>{renderCustomer(element.customer).email}</TableCell>
                            <TableCell>{renderCustomer(element.customer).phone}</TableCell>
                            <TableCell>
                                <VisibilityIcon onClick={() => onViewClick(element)} style={{ color: "black", marginRight: "5px" }} />
                            </TableCell>
                        </TableRow>
                    })}
                </TableBody>
            </Table>
            <Modal open={openModal} onClose={closeModal}>
                <Box sx={modalStyle}>
                    <Grid container>
                        <Grid item xs={8} style={{ textAlign: "right", paddingRight: "30px" }}>
                            <Typography fontWeight="bold" variant="h6"> ORDER INFORMATION</Typography>
                        </Grid>
                        <Grid item xs={4} style={{ textAlign: "right" }}>
                            <Tooltip title="Setting"><SettingsIcon onClick={onSettingClick} /></Tooltip>
                            <Menu anchorEl={anchorEl} open={openMenu} onClose={closeMenu}>
                                <MenuItem onClick={onEditClick}> <EditIcon className="mx-2" /> Edit </MenuItem>
                                <MenuItem> <DeleteIcon className="mx-2" /> Delete </MenuItem>
                            </Menu>
                        </Grid>
                    </Grid>
                    <Grid container className="my-4">
                        <Grid item xs={4}>
                            <TextField disabled value={customer.fullname} style={inputStyle} size="small" fullWidth label="Full name" variant="outlined"></TextField>
                        </Grid>
                        <Grid item xs={4}>
                            <TextField disabled value={customer.email} style={inputStyle} size="small" fullWidth label="Email" variant="outlined"></TextField>
                        </Grid>
                        <Grid item xs={4}>
                            <TextField disabled value={customer.phone} style={inputStyle} size="small" fullWidth label="Phone" variant="outlined"></TextField>
                        </Grid>
                    </Grid>
                    <Grid container className="my-4">
                        <Grid item xs={4}>
                            <TextField disabled value={customer.address} style={inputStyle} size="small" fullWidth label="Address" variant="outlined"></TextField>
                        </Grid>
                        <Grid item xs={4}>
                            <TextField disabled value={customer.city} style={inputStyle} size="small" fullWidth label="City" variant="outlined"></TextField>
                        </Grid>
                        <Grid item xs={4}>
                            <TextField disabled value={customer.country} style={inputStyle} size="small" fullWidth label="Country" variant="outlined"></TextField>
                        </Grid>
                    </Grid>
                    <Grid container className="my-4">
                        <Grid item xs={3}>
                            <TextField onChange={onStatusChange} select value={orderSelected.orderStatus} disabled={disable} style={inputStyle} size="small" fullWidth label="Order Status" variant="outlined">
                            {statusList.map((element, index) => {
                                return <MenuItem key={index} value={element.statusId}> {element.statusName} </MenuItem>
                            })}
                            </TextField>
                        </Grid>
                        <Grid item xs={3}>
                            <TextField onChange={onShipDateChange} value={orderSelected.orderStatus === 0 ? "Not yet delivery" : orderSelected.shippedDate} disabled={disable} style={inputStyle} size="small" fullWidth label="Delivery date" variant="outlined"></TextField>
                        </Grid>
                        <Grid item xs={3}>
                            <TextField value={orderSelected.timeCreated} disabled style={inputStyle} size="small" fullWidth label="Time created" variant="outlined"></TextField>
                        </Grid>
                        <Grid item xs={3}>
                            <TextField value={orderSelected.timeUpdated} disabled style={inputStyle} size="small" fullWidth label="Time updated" variant="outlined"></TextField>
                        </Grid>
                    </Grid>
                    <Grid container>
                        <Grid item xs={4} style={inputStyle}>
                            <Stack spacing={2}>
                                <Autocomplete
                                    fullWidth size="small" onChange={onChangeProduct} disabled={disable}
                                    disablePortal
                                    value={productSelected}
                                    options={allProduct}
                                    getOptionLabel={(option) => `${option.brand} ${option.name} ${option.capacity} ${option.capacity === "" ? "" : "ML"}`}
                                    renderInput={(params) => <TextField {...params} label="Product" />}
                                />
                                <TextField disabled={disable} onChange={onAmountChange} size="small" label="Amount" variant="outlined" fullWidth ></TextField>
                                <TextField value={total.toLocaleString()} disabled size="small" label="Total" variant="outlined" fullWidth ></TextField>
                                <TextField disabled={disable} onChange={onNoteChange} size="small" label="Note" variant="outlined" fullWidth ></TextField>
                                <Button onClick={onAddClick} sx={{ width: "100%" }} variant="contained">Add</Button>
                            </Stack>
                        </Grid>
                        <Grid item xs={8}>
                            <Grid container>
                                {orderDetails.map((element, index) => {
                                    return <Grid key={index} item xs={4} sx={{ paddingRight: "10px" }}>
                                        <Grid container>
                                            <Grid item xs={4}>
                                                <img style={{ width: "100px", height: "100px" }} src={renderProduct(element.product)[0].imageUrl} />
                                            </Grid>
                                            <Grid item xs={7} paddingTop="5px">
                                                <Typography variant="body2">{renderProduct(element.product)[0].brand} {renderProduct(element.product)[0].name} {renderProduct(element.product)[0].capacity}ML</Typography>
                                                <Typography variant="body2">Price: {renderProduct(element.product)[0].promotionPrice}</Typography>
                                                <Typography variant="body2">Quantity: {element.quantity}</Typography>
                                            </Grid>
                                            <Grid item xs={1} paddingTop="5px">
                                                {disable ? null : <RemoveCircleIcon onClick={() => onRemoveDetail(element)} />}
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                })}
                                {cart.map((element, index) => {
                                    return <Grid key={index} item xs={4} sx={{ paddingRight: "10px" }}>
                                        <Grid container>
                                            <Grid item xs={4}>
                                                <img style={{ width: "100px", height: "100px" }} src={renderProduct(element.product)[0].imageUrl} />
                                            </Grid>
                                            <Grid item xs={7} paddingTop="5px">
                                                <Typography variant="body2">{renderProduct(element.product)[0].brand} {renderProduct(element.product)[0].name} {renderProduct(element.product)[0].capacity}ML</Typography>
                                                <Typography variant="body2">Price: {renderProduct(element.product)[0].promotionPrice}</Typography>
                                                <Typography variant="body2">Quantity: {element.quantity}</Typography>
                                            </Grid>
                                            <Grid item xs={1} paddingTop="5px">
                                                {disable ? null : <RemoveCircleIcon onClick={() => onRemoveCart(element)} />}
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                })}
                            </Grid>

                        </Grid>
                    </Grid>


                    <Grid container >
                        <Grid item xs={12} style={{ textAlign: "right" }}>
                            {disable ? null : <Button onClick={onUpdateClick} style={{ fontWeight: "bold" }} variant="contained" color="success">Update</Button>}
                            <Button onClick={closeModal} className="mx-3" style={{ fontWeight: "bold", backgroundColor: "grey" }} variant="contained">Cancel</Button>
                        </Grid>
                    </Grid>
                </Box>
            </Modal>
            <Snackbar open={openSuccess} autoHideDuration={6000} onClose={closeSuccess}>
                <Alert onClose={closeSuccess} severity="success" sx={{ width: '100%' }}>
                    Update customer successful!
                </Alert>
            </Snackbar>
            <Snackbar open={openAlertProduct} autoHideDuration={6000} onClose={closeAlertProduct}>
                <Alert onClose={closeAlertProduct} severity="warning" sx={{ width: '100%' }}>
                    <Typography>{productError.product === "none" ? null : productError.product}</Typography>
                    <Typography>{productError.amount === "none" ? null : productError.amount}</Typography>
                </Alert>
            </Snackbar>
        </div>
    )
}

export default OrderTable