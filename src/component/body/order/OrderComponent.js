import { Grid, TextField, Pagination, MenuItem, Autocomplete } from "@mui/material"
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios"
import AddOrder from "./add order/AddOrder";
import OrderTable from "./order table/OrderTable";

function filterOrder(list, status, customer) {
    let result = list.filter(element => (element.customer === customer._id || customer._id === "") &&
        (element.orderStatus === status || status === 3));

    return result;
}

function OrderComponent() {
    const dispatch = useDispatch();
    const crud = useSelector(state => state.crud.crud);
    const [orderInPage, setOrderInPage] = useState([])
    const [search, setSearch] = useState("");
    const limit = 20;
    const [page, setPage] = useState(1);
    const [pageNo, setPageNo] = useState(0);
    const allCustomer = useSelector(state => state.customer.allCustomer);
    const [statusSelect, setStatusSelect] = useState(3)

    const statusList = [
        {statusName: "All", statusId: 3},
        {statusName: "Delivering", statusId: 0},
        {statusName: "Devlired", statusId: 1}
    ]

    const defaultCustomerValue = {
        _id: "",
        fullname: "",
        email: "",
        phone: "",
        address: "",
        city: "",
        country: "",
    }
    const [customerSelected, setCustomerSelected] = useState(defaultCustomerValue)

    const onChangeCustomer = (event, newvalue) => {
        if (newvalue === null) {
            setCustomerSelected(defaultCustomerValue);
        } else {
            setCustomerSelected(newvalue);
            console.log(newvalue)
        }
    }

    const onStatusChange = (event) => {
        setStatusSelect(event.target.value)
    }

    const onPageChange = (event, value) => {
        setPage(value)
    }

    const getAllOrder = async() => {
        const response = await axios(`http://localhost:8000/orders`);
        const data = response.data;
        return data;
    }

    useEffect(() => {
        getAllOrder()
            .then((data) => {
                dispatch({ type: "SET_ALL_ORDER", payload: data.orderList })
                let filterOrderList = filterOrder(data.orderList, statusSelect, customerSelected);
                setOrderInPage(filterOrderList.slice(limit * (page - 1), limit * page))
                setPageNo(Math.ceil(filterOrderList.length / limit))
            })
    }, [crud])

    useEffect(() => {
        getAllOrder()
            .then((data) => {
                dispatch({ type: "SET_ALL_ORDER", payload: data.orderList })
                let filterOrderList = filterOrder(data.orderList, statusSelect, customerSelected);
                setOrderInPage(filterOrderList.slice(limit * (page - 1), limit * page))
                setPageNo(Math.ceil(filterOrderList.length / limit))
                window.scrollTo(0, 5)
            })
    }, [customerSelected, statusSelect, page])

    useEffect(() => {
        setPage(1)
    }, [customerSelected, statusSelect])

    return (
        <div>
            <h1 className="text-center">List of Order </h1>
            <Grid container>
                <Grid item xs={2} style={{marginRight:"20px"}}>
                    <AddOrder></AddOrder>
                </Grid>
                <Grid item xs={3} paddingTop="15px" sx={{paddingRight:"20px"}}>
                    <TextField select value={statusSelect} onChange={onStatusChange} variant="outlined" fullWidth size="small" label="Order status">
                        {statusList.map((element, index) => {
                            return <MenuItem key={index} value={element.statusId}>{element.statusName}</MenuItem>
                        })}
                    </TextField>
                </Grid>
                <Grid item xs={6} paddingTop="15px">
                <Autocomplete
                                fullWidth size="small" onChange={onChangeCustomer}
                                disablePortal
                                value={customerSelected}
                                options={allCustomer}
                                getOptionLabel={(option) => `${option.fullname} ${option.email} ${option.phone}`}
                                renderInput={(params) => <TextField {...params} label="Customer" />}
                            />
                </Grid>
            </Grid>
            <OrderTable orderInPage={orderInPage} limit={limit} page={page}></OrderTable>
            <Pagination sx={{ float: "right" }} className="my-3" page={page} count={pageNo} onChange={onPageChange} />
        </div>
    )
}

export default OrderComponent