import { Button, Modal, Box, Typography, Grid, TextField, Alert, Snackbar, Autocomplete, Stack } from "@mui/material"
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import { useState, useEffect } from "react";
import axios from "axios";
import { useSelector, useDispatch } from "react-redux";
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';

const modalStyle = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 1100,
    height: 700,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const inputStyle = {
    paddingRight: "20px"
}


function AddOrder() {
    const allProduct = useSelector(state => state.product.allProduct)
    const allCustomer = useSelector(state => state.customer.allCustomer)
    const dispatch = useDispatch();
    const crud = useSelector(state => state.crud.crud);
    const [openModal, setOpenModal] = useState(false);
    const [openSuccess, setOpenSuccess] = useState(false);
    const [cart, setCart] = useState([])
    const [amount, setAmount] = useState();
    const [note, setNote] = useState("")
    const [openAlertProduct, setOpenAlertProduct] = useState(false);
    const [openAlertOrder, setOpenAlertOrder] = useState(false);
    const [total, setTotal] = useState(0)

    const defaultCustomerValue = {
        fullname: "",
        email: "",
        phone: "",
        address: "",
        city: "",
        country: "",
    }
    const [customerSelected, setCustomerSelected] = useState(defaultCustomerValue)

    const defaultProductValue = {
        name: "",
        brand: "",
        capacity: ""
    }
    const [productSelected, setProductSelected] = useState(defaultProductValue)

    const [productError, setProductError] = useState({
        product: "You haven't chose the product yet!",
        amount: "You haven't chose the amount yet!"
    })

    const [orderError, setOrderError] = useState({
        customer: "You haven't chose the customer yet!",
        product: "The product is empty, please choose at least one product"
    })

    const onChangeCustomer = (event, newvalue) => {
        if (newvalue === null) {
            setCustomerSelected(defaultCustomerValue);
            setOrderError({ ...orderError, customer: "You haven't chose the customer yet!" })
        } else {
            setCustomerSelected(newvalue);
            setOrderError({ ...orderError, customer: "none" })
        }
    }

    const onChangeProduct = (event, newvalue) => {
        if (newvalue === null) {
            setProductSelected(defaultProductValue);
            setProductError({ ...productError, product: "You haven't chose the product yet!" })
        } else {
            setProductSelected(newvalue);
            setProductError({ ...productError, product: "none" })
        }
    }

    const onAmountChange = (event) => {
        setAmount(event.target.value)
        if (event.target.value === "") {
            setProductError({ ...productError, amount: "You haven't chose the amount yet!" })
        } else if (isNaN(event.target.value)) {
            setProductError({ ...productError, amount: "The amount must be a number" })
        } else if (event.target.value < 1) {
            setProductError({ ...productError, amount: "The amount must be greater than 0" })
        } else {
            setProductError({ ...productError, amount: "none" })
        }
    }

    const onNoteChange = (event) => {
        setNote(event.target.value)
    }

    const onAddClick = () => {
        if (productError.product === "none" && productError.amount === "none") {
            let obj = {
                product: productSelected,
                quantity: amount
            }
            let newCart = [...cart]
            newCart.push(obj)
            setCart(newCart)
            setTotal(total + (productSelected.promotionPrice * amount))
        } else {
            setOpenAlertProduct(true)
        }
    }

    const onRemoveClick = (paramCart) => {
        let newCart = cart.filter(element => element.product._id !== paramCart.product._id);
        setCart(newCart);
        let price = paramCart.product.promotionPrice * paramCart.quantity
        setTotal(total - price)
    }

    const closeModal = () => {
        setOpenModal(false);
        setCustomerSelected(defaultCustomerValue)
        setProductSelected(defaultProductValue);
        setCart([]);
        setOrderError({
            customer: "You haven't chose the customer yet!",
            product: "The product is empty, please choose at least one product"
        })
        setProductError({
            product: "You haven't chose the product yet!",
            amount: "You haven't chose the amount yet!"    
        })
    }

    const closeSuccess = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpenSuccess(false);
    };

    const closeAlertProduct = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpenAlertProduct(false);
    };

    const closeAlertOrder = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpenAlertOrder(false);
    };

    const createNewOrder = async () => {
        let body = {
            customer: customerSelected._id,
            orderValue: total,
            note: note,
        }
        const response = await axios(`http://localhost:8000/customers/${customerSelected._id}/orders`, {
            method: `POST`,
            data: body,
            header: { "Content-Type": "application/json;charset=UTF-8" }
        })
        const data = response.data;
        return data;
    }

    const createNewOrderDetail = async(cart, paramId) => {
        let body = {
            product : cart.product._id,
            quantity :cart.quantity,
            priceEach : cart.product.promotionPrice
        }
        const response = await axios(`http://localhost:8000/orders/${paramId}/orderdetails/${cart.product._id}`, {
            method: `POST`,
            data: body,
            header: { "Content-Type": "application/json;charset=UTF-8" }
        })
        const data = response.data;
        return data;
    }

    const onInsertClick = () => {
        if (orderError.customer === "none" && orderError.product === "none") {
            setOpenSuccess(true)
            closeModal()
            createNewOrder()
                .then((data) => {
                    dispatch({ type: "RELOAD_ALL_DATA", payload: crud + 1 })
                    for(let bI = 0; bI < cart.length; bI++){
                        createNewOrderDetail(cart[bI], data.newOrder._id)
                        .then((data) => console.log(data))
                    }
                })
        } else {
            setOpenAlertOrder(true)
        }
    }

    useEffect(() => {
        if (cart.length === 0) {
            setTotal(0)
            setOrderError({ ...orderError, product: "The product is empty, please choose at least one product" })
        } else {
            setOrderError({ ...orderError, product: "none" })
        }
    }, [cart])

    return (
        <div>
            <Button onClick={() => setOpenModal(true)} className="my-3" color="success" variant="contained"> <AddCircleOutlineIcon style={{ marginRight: "5px" }} />Add new order</Button>
            <Modal open={openModal} onClose={closeModal}>
                <Box sx={modalStyle}>
                    <Typography className="text-center" variant="h6">
                        Insert New Order
                    </Typography>
                    <Grid container className="my-4">
                        <Grid item xs={12} style={inputStyle}>
                            <Autocomplete
                                fullWidth size="small" onChange={onChangeCustomer}
                                disablePortal
                                value={customerSelected}
                                options={allCustomer}
                                getOptionLabel={(option) => `${option.fullname} ${option.email} ${option.phone}`}
                                renderInput={(params) => <TextField {...params} label="Customer" />}
                            />
                        </Grid>
                    </Grid>
                    <Grid container className="my-4">
                        <Grid item xs={4}>
                            <TextField disabled value={customerSelected.fullname} style={inputStyle} size="small" fullWidth label="Full name" variant="outlined"></TextField>
                        </Grid>
                        <Grid item xs={4}>
                            <TextField disabled value={customerSelected.email} style={inputStyle} size="small" fullWidth label="Email" variant="outlined"></TextField>
                        </Grid>
                        <Grid item xs={4}>
                            <TextField disabled value={customerSelected.phone} style={inputStyle} size="small" fullWidth label="Phone" variant="outlined"></TextField>
                        </Grid>
                    </Grid>
                    <Grid container className="my-4">
                        <Grid item xs={4}>
                            <TextField disabled value={customerSelected.address} style={inputStyle} size="small" fullWidth label="Address" variant="outlined"></TextField>
                        </Grid>
                        <Grid item xs={4}>
                            <TextField disabled value={customerSelected.city} style={inputStyle} size="small" fullWidth label="City" variant="outlined"></TextField>
                        </Grid>
                        <Grid item xs={4}>
                            <TextField disabled value={customerSelected.country} style={inputStyle} size="small" fullWidth label="Country" variant="outlined"></TextField>
                        </Grid>
                    </Grid>
                    <Grid container>
                        <Grid item xs={4} style={inputStyle}>
                            <Stack spacing={2}>
                                <Autocomplete
                                    fullWidth size="small" onChange={onChangeProduct} 
                                    disablePortal
                                    value={productSelected}
                                    options={allProduct}
                                    getOptionLabel={(option) => `${option.brand} ${option.name} ${option.capacity} ${option.capacity === "" ? "" : "ML"}`}
                                    renderInput={(params) => <TextField {...params} label="Product" />}
                                />
                                <TextField onChange={onAmountChange} size="small" label="Amount" variant="outlined" fullWidth ></TextField>
                                <TextField value={total.toLocaleString()} disabled size="small" label="Total" variant="outlined" fullWidth ></TextField>
                                <TextField onChange={onNoteChange} size="small" label="Note" variant="outlined" fullWidth ></TextField>
                                <Button onClick={onAddClick} sx={{ width: "100%" }} variant="contained">Add</Button>
                            </Stack>
                        </Grid>
                        <Grid item xs={8}>
                            <Grid container>
                                {cart.map((element, index) => {
                                    return <Grid key={index} item xs={4} sx={{ paddingRight: "10px" }}>
                                        <Grid container>
                                            <Grid item xs={4}>
                                                <img style={{ width: "100px", height: "100px" }} src={element.product.imageUrl} />
                                            </Grid>
                                            <Grid item xs={7} paddingTop="5px">
                                                <Typography variant="body2">{element.product.brand} {element.product.name} {element.product.capacity}ML</Typography>
                                                <Typography variant="body2">Price: {element.product.promotionPrice.toLocaleString()}</Typography>
                                                <Typography variant="body2">Quantity: {element.quantity}</Typography>
                                            </Grid>
                                            <Grid item xs={1} paddingTop="5px">
                                                <RemoveCircleIcon onClick={() => onRemoveClick(element)} />
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                })}
                            </Grid>

                        </Grid>
                    </Grid>

                    <Grid container marginTop="20px">
                        <Grid item xs={12} style={{ textAlign: "right" }}>
                            <Button onClick={onInsertClick} style={{ fontWeight: "bold" }} variant="contained" color="success">Insert</Button>
                            <Button onClick={closeModal} className="mx-3" style={{ fontWeight: "bold", backgroundColor: "grey" }} variant="contained">Cancel</Button>
                        </Grid>
                    </Grid>
                </Box>
            </Modal>
            <Snackbar open={openSuccess} autoHideDuration={6000} onClose={closeSuccess}>
                <Alert onClose={closeSuccess} severity="success" sx={{ width: '100%' }}>
                    Insert new product successful!
                </Alert>
            </Snackbar>
            <Snackbar open={openAlertProduct} autoHideDuration={6000} onClose={closeAlertProduct}>
                <Alert onClose={closeAlertProduct} severity="warning" sx={{ width: '100%' }}>
                    <Typography>{productError.product === "none" ? null : productError.product}</Typography>
                    <Typography>{productError.amount === "none" ? null : productError.amount}</Typography>
                </Alert>
            </Snackbar>
            <Snackbar open={openAlertOrder} autoHideDuration={6000} onClose={closeAlertOrder}>
                <Alert onClose={closeAlertOrder} severity="warning" sx={{ width: '100%' }}>
                    <Typography>{orderError.product === "none" ? null : orderError.product}</Typography>
                    <Typography>{orderError.customer === "none" ? null : orderError.customer}</Typography>
                </Alert>
            </Snackbar>
        </div>
    )
}

export default AddOrder