import { Grid, AppBar, Typography } from "@mui/material"
import DehazeIcon from '@mui/icons-material/Dehaze';
import NotificationsIcon from '@mui/icons-material/Notifications';
import PersonIcon from '@mui/icons-material/Person';
const header = {
    height: "70px",
    paddingTop:"20px"
}

function HeaderComponent() {
    return (
        <div>
            <AppBar style={header} position="fixed" sx={{ top: 0, bottom: 'auto' }}>
                <Grid container>
                    <Grid item xs={1} style={{textAlign:"center"}}>
                        <DehazeIcon fontSize="large"></DehazeIcon>
                    </Grid>
                    <Grid item xs={2} style={{paddingLeft:"40px"}}>
                        <Typography variant="h6">Home Page</Typography>
                    </Grid>
                    <Grid item xs={9} style={{textAlign:"right", paddingRight:"40px"}}>
                        <NotificationsIcon className="mx-3" fontSize="large"></NotificationsIcon>
                        <PersonIcon fontSize="large"></PersonIcon>
                    </Grid>
                </Grid>
            </AppBar>
        </div>
    )
}

export default HeaderComponent