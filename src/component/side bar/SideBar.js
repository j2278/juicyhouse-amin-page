import { Stack, Typography } from "@mui/material"
import ShoppingBagIcon from '@mui/icons-material/ShoppingBag';
import AccessibilityNewIcon from '@mui/icons-material/AccessibilityNew';
import ProductionQuantityLimitsIcon from '@mui/icons-material/ProductionQuantityLimits';
import { useNavigate } from "react-router-dom";

const sideBar = {
    width:"150px",
    height: "100%",
    backgroundColor: "white",
    paddingTop: "100px",
    paddingLeft: "10px",
    position: "fixed",
    borderRight: "1px solid #CACFD2",
}

function SideBar() {
    const navigation = useNavigate();

    const onOrderClick = () => {
        navigation("/order")
    }

    const onCustomerClick = () => {
        navigation("/customer")
    }

    const onProductClick = () => {
        navigation("/product")
    }


    return (
        <div style={sideBar}>
            <Stack spacing={4}>
                <Typography fontWeight="bold" type="button" onClick={onOrderClick} style={{color:"#797D7F"}}> <ShoppingBagIcon style={{marginBottom:"10px"}}  fontSize="large"/> Order </Typography>
                <Typography fontWeight="bold" type="button" onClick={onCustomerClick} style={{color:"#797D7F "}}> <AccessibilityNewIcon style={{marginBottom:"10px"}}  fontSize="large"/> Customer </Typography>
                <Typography fontWeight="bold" type="button" onClick={onProductClick} style={{color:"#797D7F "}}> <ProductionQuantityLimitsIcon style={{marginBottom:"10px"}}  fontSize="large"/> Product </Typography>
            </Stack>
        </div>
    )
}

export default SideBar