import { Container } from "@mui/material";
import "bootstrap/dist/css/bootstrap.min.css"
import { Routes, Route } from "react-router-dom";
import CustomerComponent from "./component/body/customer/CustomerComponent";
import OrderComponent from "./component/body/order/OrderComponent";
import ProductComponent from "./component/body/product/ProductComponent";
import HeaderComponent from "./component/header/HeaderComponent";
import SideBar from "./component/side bar/SideBar";
import axios from "axios"
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";

function App() {
  const dispatch = useDispatch();

  const getAllCustomer = async () => {
    const response = await axios(`http://localhost:8000/customers`);
    const data = response.data;
    return data;
  }

  const getAllProduct = async () => {
    const response = await axios(`http://localhost:8000/products`);
    const data = response.data;
    return data;
  }

  const getAllOrder = async () => {
    const response = await axios(`http://localhost:8000/orders`);
    const data = response.data;
    return data;
  }

  useEffect(() => {
    getAllCustomer()
      .then((data) => {
        dispatch({ type: "SET_ALL_CUSTOMER", payload: data.customerList })
      })

    getAllProduct()
      .then((data) => {
        dispatch({ type: "SET_ALL_PRODUCT", payload: data.productList })
      })

    getAllOrder()
      .then((data) => {
        dispatch({ type: "SET_ALL_ORDER", payload: data.orderList })
      })
  }, [])


  return (
    <div>
      <HeaderComponent></HeaderComponent>
      <SideBar></SideBar>
      <Container style={{ marginLeft: "150px", paddingTop: "100px" }}>
        <Routes>
          <Route exact path="/order" element={<OrderComponent></OrderComponent>}></Route>
          <Route exact path="/customer" element={<CustomerComponent></CustomerComponent>}></Route>
          <Route exact path="/product" element={<ProductComponent></ProductComponent>}></Route>
        </Routes>
      </Container>
    </div>
  );
}

export default App;
